#!/usr/bin/env python

from polarizationsolver import readers
from polarizationsolver import solvers
from polarizationsolver import fields
import argparse
import numpy as np

parser = argparse.ArgumentParser(
    description="Calculate electrostatic potential in specified points")
parser.add_argument("-i",
                    "--pot",
                    dest="pot",
                    type=str,
                    required=True,
                    help="Location of potential file")
parser.add_argument(
    "-g",
    "--grid",
    dest="grid",
    type=str,
    required=True,
    help=
    "Location of file specifying location of points in which the ESP is to be\
        evaluated. The first line should contain the number of points, and the following lines the x,y,z coordinates of the points in bohr.",
)
parser.add_argument("-o",
                    "--out",
                    dest="out",
                    type=str,
                    default="ESP.dat",
                    help="Location of file to write output ESP")
parser.add_argument("--solver",
                    dest="solver",
                    type=str,
                    default="GS/DIIS",
                    choices=["GS/DIIS", "JI/DIIS", "GS", "Jacobi"],
                    help="Which induced moment solver to use")
parser.add_argument("--tolerance",
                    dest="tolerance",
                    type=float,
                    default=1e-10,
                    help="Tolerance in induced moment solver")
parser.add_argument("--damp-induced",
                    dest="damp_induced",
                    type=str,
                    default="",
                    help="Enable damping of induced moments",
                    choices=["", "thole-amoeba", "thole-exp"])
parser.add_argument("--damp-permanent",
                    dest="damp_permanent",
                    type=str,
                    default="",
                    help="Enable damping of permanent moments",
                    choices=["", "thole-amoeba", "thole-exp"])
parser.add_argument("--damping-factor",
                    dest="damp_factor",
                    type=float,
                    default=2.103,
                    help="Induced damping factor for thole-exp")
parser.add_argument("--verbose",
                    dest="verbose",
                    default=False,
                    action="store_true",
                    help="Print out more information")

# load in pot file and solve for induced moments
args = parser.parse_args()
grid = np.loadtxt(args.grid, skiprows=1)
ESP = np.zeros((grid.shape[0], 4))  # x y z ESP(x,y,z)
data = readers.potreader(args.pot)
system = readers.parser(data)
if args.damp_induced:
    system.get_induced_damping_factors(args.damp_factor)
solvers.iterative_solver(system,
                         scheme=args.solver,
                         tol=args.tolerance,
                         damp_induced=args.damp_induced,
                         damp_permanent=args.damp_permanent,
                         verbose=args.verbose)

# ESP
field_rank = 0
# (Ncoords,1,3) (-) (1,Ngrid,3) --> (Ncoords,Ngrid,3)

batch_size = 1000
num_batches = ESP.shape[0] // batch_size
for b in range(num_batches):
    print(f"{b+1}/{num_batches}")
    if b < num_batches - 1:
        Rab = -(system.coordinates[:, np.newaxis, :] -
                grid[np.newaxis, b * batch_size:(b + 1) * batch_size, :])
    else:
        Rab = -(system.coordinates[:, np.newaxis, :] -
                grid[np.newaxis, b * batch_size:, :])
    # from permanent multipole moments
    if b < num_batches - 1:
        for multipole_rank in system.active_permanent_multipole_ranks:
            ESP[b * batch_size:(b + 1) * batch_size, 3] += fields.field(
                Rab, multipole_rank, system.permanent_moments[multipole_rank],
                field_rank)
        # from induced multipole moments
        for multipole_rank in system.active_induced_multipole_ranks:
            ESP[b * batch_size:(b + 1) * batch_size, 3] += fields.field(
                Rab, multipole_rank, system.induced_moments[multipole_rank],
                field_rank)
    else:
        for multipole_rank in system.active_permanent_multipole_ranks:
            ESP[b * batch_size:, 3] += fields.field(
                Rab, multipole_rank, system.permanent_moments[multipole_rank],
                field_rank)
        # from induced multipole moments
        for multipole_rank in system.active_induced_multipole_ranks:
            ESP[b * batch_size:, 3] += fields.field(
                Rab, multipole_rank, system.induced_moments[multipole_rank],
                field_rank)

# the sign is inverted in the normal definition of the electrostatic potential
ESP = -ESP
ESP[:, 0:3] = grid
np.savetxt(args.out, ESP)
