# PolarizationSolver

This program is an induced moment solver. The underlying model is essentially the classical part of the Polarizable Embedding (PE) model.
Arbitrary-order multipole moments (charges, dipoles, quadrupoles, ...) are supported, along arbitrary order atomic (hyper-)polarizabilities (dipole-dipole,
dipole-quadrupole, ..., dipole-dipole-dipole,...). Both local and non-local polarizabilities are supported.
Potential file readers for the DALTON .pot file format are available.
An example usage of this is shown below:
~~~~~python
from readers import potreader, parser
data = potreader("mypotfile.pot")
u = parser(data)
~~~~~

Solving the system for the induced moments can then done by calling the iterative solver:
~~~~~python
from solvers import iterative_solver
u_final = iterative_solver(u)
print(u_final.induced_moments)
~~~~~
