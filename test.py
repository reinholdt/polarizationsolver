#!/usr/bin/env python
import numpy as np
from polarizationsolver.universe import system, atom, polarizability
from polarizationsolver.readers import potreader, parser
from polarizationsolver.solvers import iterative_solver
import cProfile
import sys
import time
import copy
import fields


def main():
    data = potreader("PNP1sph_TER_2261.pot")
    # data = potreader("hyper_charge.pot")
    u = parser(data)
    # u.external_field = np.array([0.001, 0.0, 0.0])

    t1 = time.time()
    iterative_solver(u, scheme="JI/DIIS")
    iterative_solver(u, scheme="GS")

    # for i in u.induced_moments[1]:
    # print("{: .9f}, {: .9f}, {: .9f}".format(*i))
    total = np.sum(u.induced_moments[1], axis=0)
    print(total)


if __name__ == "__main__":
    main()
