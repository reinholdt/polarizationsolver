#!/usr/bin/env python

import numpy as np
import argparse

symbol2number = {
    "H": 1,
    "He": 2,
    "Li": 3,
    "Be": 4,
    "B": 5,
    "C": 6,
    "N": 7,
    "O": 8,
    "F": 9,
    "Ne": 10,
    "Na": 11,
    "Mg": 12,
    "Al": 13,
    "Si": 14,
    "P": 15,
    "S": 16,
    "Cl": 17,
    "Ar": 18,
    "K": 19,
    "Ca": 20,
    "Sc": 21,
    "Ti": 22,
    "V": 23,
    "Cr": 24,
    "Mn": 25,
    "Fe": 26,
    "Co": 27,
    "Ni": 28,
    "Cu": 29,
    "Zn": 30,
    "Ga": 31,
    "Ge": 32,
    "As": 33,
    "Se": 34,
    "Br": 35,
    "Kr": 36,
    "Rb": 37,
    "Sr": 38,
    "Y": 39,
    "Zr": 40,
    "Nb": 41,
    "Mo": 42,
    "Tc": 43,
    "Ru": 44,
    "Rh": 45,
    "Pd": 46,
    "Ag": 47,
    "Cd": 48,
    "In": 49,
    "Sn": 50,
    "Sb": 51,
    "Te": 52,
    "I": 53,
    "Xe": 54,
    "Cs": 55,
    "Ba": 56,
    "La": 57,
    "Ce": 58,
    "Pr": 59,
    "Nd": 60,
    "Pm": 61,
    "Sm": 62,
    "Eu": 63,
    "Gd": 64,
    "Tb": 65,
    "Dy": 66,
    "Ho": 67,
    "Er": 68,
    "Tm": 69,
    "Yb": 70,
    "Lu": 71,
    "Hf": 72,
    "Ta": 73,
    "W": 74,
    "Re": 75,
    "Os": 76,
    "Ir": 77,
    "Pt": 78,
    "Au": 79,
    "Hg": 80,
    "Tl": 81,
    "Pb": 82,
    "Bi": 83,
    "Po": 84,
    "At": 85,
    "Rn": 86,
    "Fr": 87,
    "Ra": 88,
    "Ac": 89,
    "Th": 90,
    "Pa": 91,
    "U": 92,
    "Np": 93,
    "Pu": 94,
    "Am": 95,
    "Cm": 96,
    "Bk": 97,
    "Cf": 98,
    "Es": 99,
    "Fm": 100,
    "Md": 101,
    "No": 102,
    "Lr": 103,
    "Rf": 104,
    "Db": 105,
    "Sg": 106,
    "Bh": 107,
    "Hs": 108,
    "Mt": 109,
    "Ds": 110,
    "Rg": 111,
    "Cn": 112,
    "Nh": 113,
    "Fl": 114,
    "Mc": 115,
    "Lv": 116,
    "Ts": 117,
    "Og": 118,
}
bohr2angstrom = 0.52917724900001
angstrom2bohr = 1.0 / (bohr2angstrom)


def get_cubic_grid(coordinates, grid_buffer, grid_density):
    pmin = np.min(coordinates, axis=0) - grid_buffer
    pmax = np.max(coordinates, axis=0) + grid_buffer
    npts = ((pmax - pmin) * grid_density).astype(np.int)
    origin = pmin
    Nx = npts[0]
    Ny = npts[1]
    Nz = npts[2]
    xy = 0.0
    xz = 0.0
    yx = 0.0
    yz = 0.0
    zx = 0.0
    zy = 0.0
    xrange = np.linspace(pmin[0], pmax[0], npts[0])
    yrange = np.linspace(pmin[1], pmax[1], npts[1])
    zrange = np.linspace(pmin[2], pmax[2], npts[2])
    xx = xrange[1] - xrange[0]
    yy = yrange[1] - yrange[0]
    zz = zrange[1] - zrange[0]
    increments = np.array([[xx, xy, xz], [yx, yy, yz], [zx, zy, zz]])
    xyzgrid = np.zeros((Nx * Ny * Nz, 3), dtype=np.float64)
    counter = 0
    for i in range(Nx):
        for j in range(Ny):
            for k in range(Nz):
                xyzgrid[counter, 0] = xrange[i]
                xyzgrid[counter, 1] = yrange[j]
                xyzgrid[counter, 2] = zrange[k]
                counter += 1
    return xyzgrid, origin, npts, increments


def get_cubic_grid_from_gridspec(origin, npts, increments):
    Nx = npts[0]
    Ny = npts[1]
    Nz = npts[2]
    xyzgrid = np.zeros((Nx * Ny * Nz, 3), dtype=np.float64)
    counter = 0
    for i in range(Nx):
        for j in range(Ny):
            for k in range(Nz):
                xyzgrid[counter, :] = origin + i * increments[
                    0, :] + j * increments[1, :] + k * increments[2, :]
                counter += 1
    return xyzgrid


def write_cube(filename, coordinates, origin, npts, increments, icharges,
               fcharges, grid_data):
    natoms = coordinates.shape[0]
    with open(filename, "w") as f:
        # header, comments
        f.write("Cube file\n")
        f.write("Title card\n")
        # header
        f.write("{} {} {} {}\n".format(natoms, origin[0], origin[1],
                                       origin[2]))
        # header, box info. N is number of points, ij is increments. Data for X/Y/Z axis
        f.write("{} {} {} {}\n".format(npts[0], increments[0, 0],
                                       increments[0, 1], increments[0, 2]))
        f.write("{} {} {} {}\n".format(npts[1], increments[1, 0],
                                       increments[1, 1], increments[1, 2]))
        f.write("{} {} {} {}\n".format(npts[2], increments[2, 0],
                                       increments[2, 1], increments[2, 2]))
        # header atoms
        for i in range(natoms):
            f.write("{} {} {} {} {}\n".format(icharges[i], fcharges[i],
                                              coordinates[i, 0],
                                              coordinates[i, 1],
                                              coordinates[i, 2]))

        # main data
        counter = 0
        for i in range(npts[0]):
            for j in range(npts[1]):
                for k in range(npts[2]):
                    f.write("{:.6E} ".format(grid_data[counter]))
                    if k % 6 == 5:
                        f.write("\n")
                    counter += 1
                f.write("\n")
        f.write("\n")


parser = argparse.ArgumentParser(description="")
parser.add_argument("--xyz", dest="xyz", type=str, required=True)
parser.add_argument("--data", dest="data", type=str)
parser.add_argument("--cube-density",
                    dest="cube_density",
                    default=4.0,
                    type=float,
                    help="Points/bohr to output to cube file.")
parser.add_argument(
    "--cube-buffer",
    dest="cube_buffer",
    default=5.0,
    type=float,
    help="Buffer (in bohr) to add around min/max value of coordinates")
parser.add_argument(
    "--cube-spec",
    dest="cubespec",
    type=str,
    default="",
    help="String of ORIGIN [3*float] NPTS [3*int] INCREMENTS [3*3*float]")
parser.add_argument("--write-grid", dest="write_grid", default="", type=str)
parser.add_argument("--write-cube", dest="write_cube", default="", type=str)

args = parser.parse_args()

xyz = np.loadtxt(args.xyz, skiprows=2, dtype=str)
elements = xyz[:, 0]
icharges = np.array([symbol2number[element] for element in elements],
                    dtype=np.int64)
fcharges = icharges.astype(np.float64)
coordinates = xyz[:, 1:4].astype(np.float64) * angstrom2bohr

if args.write_grid:
    if args.cubespec:
        spec = args.cubespec.split()
        origin = [float(x) for x in spec[0:3]]
        npts = [int(x) for x in spec[3:6]]
        increments = np.array([float(x) for x in spec[6:]]).reshape(3, 3)
        xyzgrid = get_cubic_grid_from_gridspec(origin, npts, increments)
    else:
        xyzgrid, origin, npts, increments = get_cubic_grid(
            coordinates, args.cube_buffer, args.cube_density)
    np.savetxt(args.write_grid,
               xyzgrid,
               header="{}".format(xyzgrid.shape[0]),
               comments=" ")
if args.write_cube:
    skip = 0
    data = np.loadtxt(args.data, skiprows=0)
    if args.cubespec:
        spec = args.cubespec.split()
        origin = [float(x) for x in spec[0:3]]
        npts = [int(x) for x in spec[3:6]]
        increments = np.array([float(x) for x in spec[6:]]).reshape(3, 3)
        xyzgrid = get_cubic_grid_from_gridspec(origin, npts, increments)
    else:
        xyzgrid, origin, npts, increments = get_cubic_grid(
            coordinates, args.cube_buffer, args.cube_density)

    assert np.allclose(xyzgrid[:, 0:3], data[:, 0:3])
    write_cube(args.write_cube, coordinates, origin, npts, increments,
               icharges, fcharges, data[:, 3])
