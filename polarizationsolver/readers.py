#!/usr/bin/env python
import sys
import itertools
import numpy as np
from .universe import system, atom, polarizability


def potreader(filename):
    """
    Reads up a .pot file and extracts the coordinates, moments and polarizabilities to a more suitable working structure
    """
    coordinates = []
    elements = []
    moments = dict()
    polarizabilities = dict()
    distributed_polarizabilities = dict()
    exclusions = dict()
    exclusions_permanent = dict()
    exclusions_induced = dict()
    thole_damping_widths = dict()
    thole_damping_factors = dict()

    read = ""
    # number of unique combinations with replacement
    multipole_length = np.array([1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120, 136])

    with open(filename, "r") as f:
        for line in f:
            if line[0] == "!":
                # comment, skip
                continue
            elif line == "@COORDINATES\n":
                # start of coordinates
                read = "coord"
                counter = 0
                continue
            elif line == "@MULTIPOLES\n":
                # start of multipoles
                read = "multi"
                counter = 0
                continue
            elif line == "@POLARIZABILITIES\n":
                # start of polarizabilities
                read = "pol"
                counter = 0
                continue
            elif line == "EXCLISTS\n":
                # exclusion lists, used to separate atoms into sites
                read = "excl"
                counter = 0
                continue
            elif line == "EXCLISTS_PERMANENT\n":
                read = "excl_permanent"
                counter = 0
                continue
            elif line == "EXCLISTS_INDUCED\n":
                read = "excl_induced"
                counter = 0
                continue
            elif line == "@THOLE\n":
                read = "thole"
                counter = 0
                continue
            elif line == "@DISTRIBUTED_POLARIZABILITIES\n":
                read = "distprop"
                counter = 0
                continue
            if read == "coord":
                if counter == 0:
                    natoms = int(line)
                elif counter == 1:
                    unit = line.split()[0]
                elif counter < natoms + 2:
                    split = line.split()
                    elements.append(split[0])
                    coordinates.append(split[1:4])
                counter += 1
            elif read == "multi":
                if "ORDER" in line:
                    counter = 0
                    order = int(line.split()[-1])
                    moments[order] = [np.zeros(multipole_length[order])] * natoms
                elif counter == 1:
                    pass
                    # natoms = int(line)
                elif counter < natoms + 2:
                    split = line.split()
                    label = split[0]
                    index = int(label) - 1
                    moments[order][index] = split[1:]
                counter += 1
            elif read == "pol":
                if "ORDER" in line:
                    counter = 0
                    split = line.split()
                    order = tuple([int(x) for x in split[1:]])
                    rank = np.sum(order)
                    polarizabilities[order] = [np.zeros(multipole_length[rank])] * natoms
                elif counter == 1:
                    pass
                elif counter < natoms + 2:
                    split = line.split()
                    label = split[0]
                    index = int(label) - 1
                    polarizabilities[order][index] = split[1:]
                counter += 1
            elif read == "excl":
                if counter == 0:
                    natoms = int(line.split()[0])
                    nexcl = int(line.split()[1])
                else:
                    entry = line.split()
                    entry = np.array(entry).astype(np.int64)
                    # remove zeroes, which corresponds to no atom excluded
                    entry = entry[entry != 0]
                    # zeroth index is the index of the atoms; next are excluded interactions. Indices are shifted by (-1) to match counting from zero
                    exclusions[entry[0] - 1] = entry - 1
                counter += 1
            elif read == "excl_permanent":
                if counter == 0:
                    natoms = int(line.split()[0])
                    nexcl = int(line.split()[1])
                else:
                    entry = line.split()
                    entry = np.array(entry).astype(np.int64)
                    # remove zeroes, which corresponds to no atom excluded
                    entry = entry[entry != 0]
                    # zeroth index is the index of the atoms; next are excluded interactions. Indices are shifted by (-1) to match counting from zero
                    exclusions_permanent[entry[0] - 1] = entry - 1
                counter += 1
            elif read == "excl_induced":
                if counter == 0:
                    natoms = int(line.split()[0])
                    nexcl = int(line.split()[1])
                else:
                    entry = line.split()
                    entry = np.array(entry).astype(np.int64)
                    # remove zeroes, which corresponds to no atom excluded
                    entry = entry[entry != 0]
                    # zeroth index is the index of the atoms; next are excluded interactions. Indices are shifted by (-1) to match counting from zero
                    exclusions_induced[entry[0] - 1] = entry - 1
                counter += 1
            elif read == "thole":
                if counter == 0:
                    natoms = int(line.split()[0])
                else:
                    split = line.split()
                    index = int(split[0]) - 1
                    a = float(split[1])
                    pdamp = float(split[2])
                    thole_damping_widths[index] = a
                    thole_damping_factors[index] = pdamp
                counter += 1
            elif read == "distprop":
                if "ORDER" in line:
                    counter = 0
                    split = line.split()
                    order = tuple([int(x) for x in split[1:]])
                    nidx = len(order)
                    rank = np.sum(order)
                    distributed_polarizabilities[order] = dict()
                elif counter == 1:
                    pass
                elif counter < natoms * natoms + 2:
                    split = line.split()
                    indices = tuple([int(x) for x in split[0:nidx]])
                    distributed_polarizabilities[order][indices] = np.array(split[nidx:]).astype(np.float64)
                counter += 1
        coordinates = np.array(coordinates).astype(np.float64)
        for key in moments.keys():
            moments[key] = np.array(moments[key])[:, :].astype(np.float64)
        for key in polarizabilities.keys():
            polarizabilities[key] = np.array(polarizabilities[key]).astype(np.float64)
        if unit == "AA":
            coordinates *= 1.88972598858
        return {
            "coordinates": coordinates,
            "elements": elements,
            "moments": moments,
            "polarizabilities": polarizabilities,
            "distributed_polarizabilities": distributed_polarizabilities,
            "exclusions": exclusions,
            "exclusions_permanent": exclusions_permanent,
            "exclusions_induced": exclusions_induced,
            "thole_damping_widths": thole_damping_widths,
            "thole_damping_factors": thole_damping_factors
        }


def fill(values, dim):
    out = np.zeros([3] * dim)
    counter = 0
    for c in itertools.combinations_with_replacement((0, 1, 2), dim):
        for index in list(set(itertools.permutations(c))):
            out[index] = values[counter]
        counter += 1
    return out


def fill_distprop(values, order):
    if len(order) == 1:
        return fill(values, order[0])
    elif len(order) == 2:
        counter = 0
        rank = np.sum(order)
        out = np.zeros([3] * rank)
        for c1 in itertools.combinations_with_replacement((0, 1, 2), order[0]):
            for c2 in itertools.combinations_with_replacement((0, 1, 2), order[1]):
                for index1 in list(set(itertools.permutations(c1))):
                    for index2 in list(set(itertools.permutations(c2))):
                        out[index1 + index2] = values[counter]
                        # print(index1, index2, counter)
                counter += 1
        return out
    elif len(order) > 2:
        raise NotImplementedError


def parser(data):
    """
    Parses coordinates, moments, polarizabilities
    data is a dictionary containing coordinates, moments, polarizabilities and molecule indices
    Returns system object
    """
    atoms = []
    if len(data["moments"]) != 0:
        max_multipole_order = np.max(list(data["moments"].keys()))
    else:
        max_multipole_order = -1

    for atomidx in range(len(data["coordinates"])):
        # 1) construct polarizabilities
        # Convention: First index of the polarizability tuple is the kind (order) of moment generated;
        # 0 - charge, 1 - dipole, 2 - quadrupole, etc...
        # Further indices are the moment-generating fields;
        # 1 - field, 2 - field gradient (quadrupole field), etc.,
        # So a dipole-dipole polarizability is       :     (1,1),
        # dipole-dipole-dipole hyperpolarizability   :   (1,1,1),
        # dipole-quadrupole polarizability           :     (1,2)
        ps = []
        for key in data["polarizabilities"].keys():
            rank = sum(key)
            p_values = data["polarizabilities"][key][atomidx]
            p_mat = fill(p_values, rank)
            p = polarizability(values=p_mat, moment_rank=key[0], field_ranks=np.array(key[1:]), field_source_idx=atomidx)
            # for standard polarizability, the source of the inducing field
            # is on the same atom as the polarizability
            ps.append(p)
        for key in data["distributed_polarizabilities"]:
            order = key
            for indices in data["distributed_polarizabilities"][key]:
                destination_idx = indices[0]
                if atomidx + 1 == destination_idx:
                    p_values = data["distributed_polarizabilities"][key][indices]
                    p_mat = fill_distprop(p_values, order)
                    p = polarizability(values=p_mat, moment_rank=key[0], field_ranks=np.array(key[1:]), field_source_idx=indices[1] - 1)
                    ps.append(p)
        # print(ps)
        # 2) construct multipoles
        moments = []
        for morder in range(max_multipole_order + 1):
            if morder in data["moments"]:
                m_mat = fill(data["moments"][morder][atomidx], morder)
                moments.append(m_mat)

        # 3) handle exclusions
        # if multiple specified: split exclusions take priority

        # always remove self-interactions
        excl_permanent = [atomidx]
        excl_induced = [atomidx]

        if atomidx in data["exclusions"].keys():
            excl_permanent += list(data["exclusions"][atomidx])
            excl_induced += list(data["exclusions"][atomidx])

        if atomidx in data["exclusions_permanent"]:
            excl_permanent += list(data["exclusions_permanent"][atomidx])
        if atomidx in data["exclusions_induced"]:
            excl_induced += list(data["exclusions_induced"][atomidx])

        excl_permanent = list(set(excl_permanent))
        excl_induced = list(set(excl_induced))

        # 4) construct atoms
        a = atom(data["coordinates"][atomidx], moments, ps, excl_permanent, excl_induced)
        atoms.append(a)

    u = system(np.array(atoms, dtype=object))
    a = data["thole_damping_widths"]
    pdamp = data["thole_damping_factors"]
    if a and pdamp:
        u.damping_factors = np.zeros((u.natoms, u.natoms))
        for i in range(u.natoms):
            for j in range(u.natoms):
                u.damping_factors[i, j] = min(a[i], a[j])**(1.0 / 3.0) / (pdamp[i] * pdamp[j])
    return u
