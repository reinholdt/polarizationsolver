import numpy as np
from string import ascii_lowercase as abc


class polarizability(object):
    def __init__(self, values, moment_rank, field_ranks, field_source_idx):
        self.values = values  # values is a numpy array containing the (non-compressed) components of the polarizability in question
        #
        self.field_ranks = field_ranks  # rank(s) of the field(s) hitting the polarizability, ex. array([1]) for dipole-dipole pol, array([1,1] for the first
        #  dipole-dipole-dipole hyperpolarizability)
        #
        # print(field_ranks)
        self.field_source_idx = field_source_idx  # index of field source location -- for canonical polarizabilities, this is the same as the atom idx;
        #  for DistProp polarizability, idx belongs to one atom in a fragment
        #
        self.moment_rank = moment_rank  # output rank of moments --> is it charge, dipole, quadrupole, etc...
        self.full_rank = np.sum(field_ranks) + moment_rank
        subsigs = []

        acc = 0
        for i in field_ranks:
            subsigs.append(abc[self.moment_rank + acc:self.moment_rank + acc + i])
            acc += i
        self.signature = "{}->{}".format(",".join([abc[:self.full_rank], *subsigs]), abc[:self.full_rank - acc])
        # signature tells us which tensor contraction to carry out for this polarizability (done with einsum)
        assert values.size == 3**self.full_rank

    def set_induced_moment_index(self, induced_moment_index):
        self.induced_moment_index = induced_moment_index


class atom(object):
    def __init__(self, coordinates, permanent_moments, polarizabilities, excluded_interactions_permanent, excluded_interactions_induced):
        self.coordinates = coordinates
        self.permanent_moments = permanent_moments
        self.polarizabilities = polarizabilities
        self.excluded_interactions_permanent = excluded_interactions_permanent
        self.excluded_interactions_induced = excluded_interactions_induced
        self.idx = -1
        self.induced_moments = []
        self.permanent_multipole_ranks = [int(np.log(m.size) / np.log(3)) for m in self.permanent_moments]
        if self.permanent_multipole_ranks:
            self.max_permanent_multipole_rank = np.max(self.permanent_multipole_ranks)
        else:
            self.max_permanent_multipole_rank = -1
        self.induced_multipole_ranks = []
        self.active_field_ranks = set()

        # find required fields as required from polarizabilities
        induced_moment_index = 0
        for pol in self.polarizabilities:
            self.active_field_ranks = self.active_field_ranks.union(set(pol.field_ranks))
            self.induced_moments.append(np.zeros([3] * pol.moment_rank))
            self.induced_multipole_ranks.append(pol.moment_rank)
            pol.set_induced_moment_index(induced_moment_index)
            induced_moment_index += 1
        self.active_field_ranks = list(self.active_field_ranks)
        if len(self.active_field_ranks) > 0:
            self.max_field_rank = np.max(self.active_field_ranks)
        else:
            self.max_field_rank = -1
        if len(self.induced_multipole_ranks) > 0:
            self.max_induced_multipole_rank = np.max(self.induced_multipole_ranks)
        else:
            self.max_induced_multipole_rank = -1

        self.induced_fields = np.empty((self.max_field_rank + 1), dtype=object)
        self.permanent_fields = np.empty((self.max_field_rank + 1), dtype=object)
        for i in range(self.max_field_rank + 1):
            self.induced_fields[i] = np.zeros([3] * i, dtype=np.float64)
            self.permanent_fields[i] = np.zeros([3] * i, dtype=np.float64)


class system(object):
    def __init__(self, atoms, external_field=None):
        self.atoms = atoms
        self.natoms = len(atoms)
        self.active_field_ranks = []
        self.active_induced_multipole_ranks = []
        self.active_permanent_multipole_ranks = []
        self.max_field_rank = 0
        self.max_induced_multipole_rank = 0
        self.max_permanent_multipole_rank = 0
        self.max_multipole_rank = 0
        self.external_field = external_field
        self.update()

    def update(self):
        iatom = 0
        for atom in self.atoms:
            atom.idx = iatom
            iatom += 1
            self.active_field_ranks = list(set(self.active_field_ranks + atom.active_field_ranks))
            self.active_induced_multipole_ranks = list(set(self.active_induced_multipole_ranks + atom.induced_multipole_ranks))
            self.active_permanent_multipole_ranks = list(set(self.active_permanent_multipole_ranks + atom.permanent_multipole_ranks))
        if len(self.active_field_ranks) > 0:
            self.max_field_rank = np.max(self.active_field_ranks)
        else:
            self.max_field_rank = -1
        if len(self.active_induced_multipole_ranks) > 0:
            self.max_induced_multipole_rank = np.max(self.active_induced_multipole_ranks)
        else:
            self.max_induced_multipole_rank = -1
        if self.active_permanent_multipole_ranks:
            self.max_permanent_multipole_rank = np.max(self.active_permanent_multipole_ranks)
        else:
            self.max_permanent_multipole_rank = -1
        self.max_multipole_rank = np.max([self.max_induced_multipole_rank, self.max_permanent_multipole_rank])
        self.coordinates = np.zeros((self.natoms, 3))
        self.permanent_moments = np.array([None] * (self.max_permanent_multipole_rank + 1), dtype=object)
        self.induced_moments = np.array([None] * (self.max_induced_multipole_rank + 1), dtype=object)

        for i in range(self.max_permanent_multipole_rank + 1):
            self.permanent_moments[i] = np.zeros((self.natoms, *[3] * i))
        for i in range(self.max_induced_multipole_rank + 1):
            self.induced_moments[i] = np.zeros((self.natoms, *[3] * i))

        for i in range(self.natoms):
            self.coordinates[i] = self.atoms[i].coordinates
            for irank, rank in enumerate(self.atoms[i].permanent_multipole_ranks):
                self.permanent_moments[rank][i] = self.atoms[i].permanent_moments[irank]

    def get_induced_damping_factors(self, damping_factor):
        self.damping_factors = np.zeros((self.natoms, self.natoms))
        for i in range(self.natoms):
            alpha_i = self.atoms[i].polarizabilities[0].values.trace() / 3
            for j in range(self.natoms):
                alpha_j = self.atoms[j].polarizabilities[0].values.trace() / 3
                self.damping_factors[i, j] = damping_factor / (alpha_i * alpha_j)**(1.0 / 6.0)
