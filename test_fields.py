import numpy as np
import os
import unittest

from polarizationsolver import tensors
from polarizationsolver import fields


def test_charge_potential():
    # unit charge at origin, potential at (0,0,1) is 1.0
    q = np.array([1.0])
    field_rank = 0
    multipole_rank = 0
    Rab = np.array([[0.0, 0.0, 1.0]])
    assert np.allclose(fields.field(Rab, multipole_rank, q, field_rank),
                       -1.0 * np.array([1.0]))


def test_charge_field():
    q = np.array([1.0])
    field_rank = 1
    multipole_rank = 0
    Rab = np.array([[0.0, 0.0, 1.0]])
    assert np.allclose(fields.field(Rab, multipole_rank, q, field_rank),
                       np.array([0, 0, 1]))


def test_dipole_field():
    mu = np.array([[0, 0, 1]])
    field_rank = 1
    multipole_rank = 1
    Rab = np.array([[0.0, 0.0, 1.0]])
    assert np.allclose(fields.field(Rab, multipole_rank, mu, field_rank),
                       np.array([0, 0, 2]))
    Rab = np.array([[0.0, 0.0, 2.0]])
    assert np.allclose(fields.field(Rab, multipole_rank, mu, field_rank),
                       np.array([0, 0, 0.25]))
