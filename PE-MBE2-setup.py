#!/usr/bin/env python

import polarizationsolver
import argparse
import pathlib
import numpy as np
import sh
from functools import partial
from polarizationsolver import readers, writers, solvers

parser = argparse.ArgumentParser(description='Set up pre-induced potential files for PE-MBE(2) calculations')
parser.add_argument('-i', '--pot',  dest='pot', type=str, required=True, help='Location of potential file')
parser.add_argument('-o', '--output-dir',  dest='output_dir', type=str, default=None, help='Location output folder')
parser.add_argument('--solver',     dest='solver', type=str, default="GS/DIIS", choices=['GS/DIIS', 'JI/DIIS', 'GS', 'Jacobi'], help='Which induced moment solver to use')
parser.add_argument('--tolerance',  dest='tolerance', type=float, default=1e-10, help='Tolerance in induced moment solver')
parser.add_argument('-s', '--subsystems', dest='subsystems', type=int, nargs='+', action='append', help='Specify subsystems to write out pre-polarized monomer/dimer potentials for')
parser.add_argument('--basis', dest='basis', type=str, default='aug-cc-pVTZ', help='Specify basis set to write in mol files')
parser.add_argument("--damp-induced",
                    dest="damp_induced",
                    type=str,
                    default="",
                    help="Enable damping of induced moments",
                    choices=["", "thole-amoeba", "thole-exp"])
parser.add_argument("--damp-permanent",
                    dest="damp_permanent",
                    type=str,
                    default="",
                    help="Enable damping of permanent moments",
                    choices=["", "thole-amoeba", "thole-exp"])
parser.add_argument("--damping-factor",
                    dest="damp_factor",
                    type=float,
                    default=2.103,
                    help="Induced damping factor for thole-exp")
parser.add_argument("--verbose",
                    dest="verbose",
                    default=False,
                    action="store_true",
                    help="Print out more information")

args = parser.parse_args()

if args.output_dir is None:
    output_dir = pathlib.Path(args.pot).stem
else:
    output_dir = args.output_dir

data = readers.potreader(args.pot)
system = readers.parser(data)
if args.damp_induced:
    system.get_induced_damping_factors(args.damp_factor)

solvers.iterative_solver(system,
                         scheme=args.solver,
                         tol=args.tolerance,
                         damp_induced=args.damp_induced,
                         damp_permanent=args.damp_permanent,
                         verbose=args.verbose)

data.pop('polarizabilities')
data['moments'][1] += system.induced_moments[1]

all_indices = set(range(len(data['coordinates'])))
n_subsystems = len(args.subsystems)

def potdata_subset(data, indices):
    reorder_dict = {}
    for i, index in enumerate(indices):
        reorder_dict[index] = i
    def reorder(index, d):
        return d[index]
    reorder = partial(reorder, d=reorder_dict)

    data_subset = {'coordinates': np.array([data['coordinates'][idx] for idx in indices]),
                   'elements': [data['elements'][idx] for idx in indices],
                   'moments': {order: [multipoles[idx] for idx in indices] for order, multipoles in data['moments'].items()},
                   'exclusions': {reorder(index): list(map(reorder, excl)) for index, excl in data['exclusions'].items() if index in indices},
                   }
    return data_subset

sh.mkdir('-p', f'{output_dir}/monomer/')
sh.mkdir('-p', f'{output_dir}/dimer/')

for i in range(n_subsystems):
    # monomer 
    subsystem_indices = set(map(lambda x: x-1, args.subsystems[i]))
    remaining_indices = all_indices - subsystem_indices
    data_subset = potdata_subset(data, remaining_indices)
    # TODO: handle charged fragments
    writers.molwriter(f'{output_dir}/monomer/{i+1}.mol',
            basis=args.basis, charge=0,
            coordinates=np.array([data['coordinates'][idx] for idx in subsystem_indices]),
            elements=[data['elements'][idx] for idx in subsystem_indices])
    writers.potwriter(f'{output_dir}/monomer/{i+1}.pot', data_subset)
    for j in range(i+1, n_subsystems):
        # dimer
        subsystem_indices = set(map(lambda x: x-1, args.subsystems[i] + args.subsystems[j]))
        remaining_indices = all_indices - subsystem_indices
        data_subset = potdata_subset(data, remaining_indices)
        writers.molwriter(f'{output_dir}/dimer/{i+1}_{j+1}.mol',
                basis=args.basis, charge=0,
                coordinates=np.array([data['coordinates'][idx] for idx in subsystem_indices]),
                elements=[data['elements'][idx] for idx in subsystem_indices])
        writers.potwriter(f'{output_dir}/dimer/{i+1}_{j+1}.pot', data_subset)
        

